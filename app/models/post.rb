class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy #delete all comments when its post is deleted
  validates_presence_of :title
  validates_presence_of :body
end
